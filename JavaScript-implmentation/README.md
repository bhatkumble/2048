# 2048

Command line interface for the 2048 game.

Make sure you have node and npm installed in your system.

Run the following command from the 2048 directory

`npm run start`
