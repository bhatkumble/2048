"use strict";

const readline = require('readline');

const cellInRow = 4;
const goal = 2048;

let movePossible = false;

let input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let direction = {
  "1": "left",
  "4": "right",
  "2": "up",
  "3": "down"
};

let playground = [];

let generatePlaygroud = (playground) => {
  let totalCells = cellInRow * cellInRow;
  for(let i = 0; i < totalCells; i++) {
    playground.push(0);
  }
  populatePlayground(playground);
  populatePlayground(playground);
  printPlayground(playground);
}

let populatePlayground = (playground) => {
  let zeroes = playground.reduce((data, value, index) => {
    if (value == 0) {
      data.push(index);
    }
    return data;
  }, []);
  if(zeroes.length == 0) {
    console.log("No more moves. Game over!!");
    input.close();
    return clearInterval(game);
  }
  let numbers = [2, 4];
  let index = zeroes[Math.floor(Math.random() * zeroes.length)];
  let numberToPopulate = numbers[Math.floor(Math.random() * numbers.length)]
  playground[index] = numberToPopulate;
}

let printPlayground = (playground) => {
  let maze = playground.map((value, index) => {
    if(index % cellInRow == 0) {
      return `${playground[index]}`;
    }
    else if (index % cellInRow == (cellInRow - 1)) {
      return `  ${playground[index]}\n`;
    }
    else {
      return `  ${playground[index]}`;
    }
  })
  console.log(` ${maze.join(' ')}`);
};

let addItems = (move) => {
  let dataCopy = playground;
  switch (move) {
    case "1":
      let mergedLeft = [];
      for(let k = 0; k < cellInRow; k++) {
        for(let i = 0; i < cellInRow; i++) {
          let indexX = cellInRow * i + k;
          for(let j = 0; j < cellInRow; j++) {
            let next = cellInRow * i + j;
            let prev = indexX - 1;
            if(k == 0 && next > indexX) {
              if (dataCopy[indexX] == 0 && dataCopy[next] !== 0) {
                dataCopy[indexX] = dataCopy[next] + dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[indexX] !== 0 && dataCopy[next] !== 0 &&
                  dataCopy[indexX] != dataCopy[next]) {
                break;
              }
              else if (dataCopy[indexX] !== 0 &&
                  dataCopy[indexX] == dataCopy[next]) {
                dataCopy[indexX] = 2 * dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
                mergedLeft.push(indexX);
                break;
              }
            }
            else if (k > 0 && next > indexX) {
              if (dataCopy[prev] == 0 && dataCopy[indexX] != 0) {
                dataCopy[prev] = dataCopy[prev] + dataCopy[indexX];
                dataCopy[indexX] = 0;
                movePossible = true;
              }
              else if (mergedLeft.indexOf(prev) == -1 && dataCopy[prev] != 0 &&
                  dataCopy[prev] == dataCopy[indexX]) {
                dataCopy[prev] = 2 * dataCopy[prev];
                dataCopy[indexX] = 0;
                movePossible = true;
                mergedLeft.push(prev);
              }
              if (dataCopy[next] != 0 && dataCopy[indexX] == 0) {
                dataCopy[indexX] = dataCopy[indexX] + dataCopy[next];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[next] != 0 &&
                  dataCopy[next] == dataCopy[indexX]) {
                dataCopy[indexX] = 2 * dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
                mergedLeft.push(indexX);
              }
            }
          }
        }
      }
      mergedLeft = [];
      break;
    
      case "2":
      let mergedUp = [];
      for(let k = 0; k < cellInRow; k++) {
        for(let i = 0; i < cellInRow; i++) {
          let indexY = cellInRow * k + i;
          for(let j = 0; j < cellInRow; j++) {
            let next = cellInRow * j + i;
            let prev = indexY - cellInRow;
            if(k == 0 & next > indexY) {
              if (dataCopy[indexY] == 0 && dataCopy[next] !== 0) {
                dataCopy[indexY] = dataCopy[next] + dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[indexY] !== 0 && dataCopy[next] !== 0 &&
                  dataCopy[indexY] != dataCopy[next]) {
                break;
              }
              else if (dataCopy[indexY] !== 0 &&
                  dataCopy[indexY] == dataCopy[next]) {
                dataCopy[indexY] = 2 * dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
                mergedUp.push(indexY);
                break;
              }
            }
            else if (k > 0 && next > indexY) {
              if (dataCopy[prev] == 0 && dataCopy[indexY] != 0) {
                dataCopy[prev] = dataCopy[prev] + dataCopy[indexY];
                dataCopy[indexY] = 0;
                movePossible = true;
              }
              else if (mergedUp.indexOf(prev) == -1 && dataCopy[prev] != 0 &&
                  dataCopy[prev] == dataCopy[indexY]) {
                dataCopy[prev] = 2 * dataCopy[prev];
                dataCopy[indexY] = 0;
                movePossible = true;
                mergedUp.push(prev);
              }
              if (dataCopy[next] != 0 && dataCopy[indexY] == 0) {
                dataCopy[indexY] = dataCopy[indexY] + dataCopy[next];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[next] != 0 &&
                  dataCopy[next] == dataCopy[indexY]) {
                dataCopy[indexY] = 2 * dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
                mergedUp.push(indexY);
              }
            }
          }
        }
      }
      mergedUp = [];
      break;

      case "4":
      let mergedRight = [];
      for(let k = cellInRow - 1; k >= 0; k--) {
        for(let i = cellInRow - 1; i >= 0; i--) {
          let indexX = cellInRow * i + k;
          for(let j = cellInRow - 1; j >= 0; j--) {
            let next = cellInRow * i + j;
            let prev = indexX + 1;
            if(k == cellInRow - 1 && next < indexX) {
              if (dataCopy[indexX] == 0 && dataCopy[next] !== 0) {
                dataCopy[indexX] = dataCopy[next] + dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[indexX] !== 0 && dataCopy[next] !== 0 &&
                  dataCopy[indexX] != dataCopy[next]) {
                break;
              }
              else if (dataCopy[indexX] !== 0 &&
                  dataCopy[indexX] == dataCopy[next]) {
                dataCopy[indexX] = 2 * dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
                mergedRight.push(indexX);
                break;
              }
            }
            else if (k < cellInRow - 1 && next < indexX) {
              if (dataCopy[prev] == 0 && dataCopy[indexX] != 0) {
                dataCopy[prev] = dataCopy[prev] + dataCopy[indexX];
                dataCopy[indexX] = 0;
                movePossible = true;
              }
              else if (mergedRight.indexOf(prev) == -1 && dataCopy[prev] != 0 &&
                  dataCopy[prev] == dataCopy[indexX]) {
                dataCopy[prev] = 2 * dataCopy[prev];
                dataCopy[indexX] = 0;
                movePossible = true;
                mergedRight.push(prev);
              }
              if (dataCopy[next] != 0 && dataCopy[indexX] == 0) {
                dataCopy[indexX] = dataCopy[indexX] + dataCopy[next];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[next] != 0 &&
                  dataCopy[next] == dataCopy[indexX]) {
                dataCopy[indexX] = 2 * dataCopy[indexX];
                dataCopy[next] = 0;
                movePossible = true;
                mergedRight.push(indexX);
              }
            }
          }
        }
      }
      mergedRight = [];
      break;

      case "3":
      let mergedDown = [];
      for(let k = cellInRow - 1; k >= 0; k--) {
        for(let i = cellInRow - 1; i >= 0; i--) {
          let indexY = cellInRow * k + i;
          for(let j = cellInRow - 1; j >= 0; j--) {
            let next = cellInRow * j + i;
            let prev = indexY + cellInRow;
            if(k == cellInRow - 1 & next < indexY) {
              if (dataCopy[indexY] == 0 && dataCopy[next] !== 0) {
                dataCopy[indexY] = dataCopy[next] + dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[indexY] !== 0 && dataCopy[next] !== 0 &&
                  dataCopy[indexY] != dataCopy[next]) {
                break;
              }
              else if (dataCopy[indexY] !== 0 &&
                  dataCopy[indexY] == dataCopy[next]) {
                dataCopy[indexY] = 2 * dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
                mergedDown.push(indexY);
                break;
              }
            }
            else if (k < cellInRow - 1 && next < indexY) {
              if (dataCopy[prev] == 0 && dataCopy[indexY] != 0) {
                dataCopy[prev] = dataCopy[prev] + dataCopy[indexY];
                dataCopy[indexY] = 0;
                movePossible = true;
              }
              else if (mergedDown.indexOf(prev) == -1 && dataCopy[prev] != 0 &&
                  dataCopy[prev] == dataCopy[indexY]) {
                dataCopy[prev] = 2 * dataCopy[prev];
                dataCopy[indexY] = 0;
                movePossible = true;
                mergedDown.push(prev);
              }
              if (dataCopy[next] != 0 && dataCopy[indexY] == 0) {
                dataCopy[indexY] = dataCopy[indexY] + dataCopy[next];
                dataCopy[next] = 0;
                movePossible = true;
              }
              else if (dataCopy[next] != 0 &&
                  dataCopy[next] == dataCopy[indexY]) {
                dataCopy[indexY] = 2 * dataCopy[indexY];
                dataCopy[next] = 0;
                movePossible = true;
                mergedDown.push(indexY);
              }
            }
          }
        }
      }
      mergedDown = [];
      break;

    default:
      break;
  }
  if(movePossible) {
    playground = dataCopy;
    checkGameEnd();
  }
}

let checkGameEnd = () => {
  if(playground.indexOf(goal) !== -1) {
    console.log("You win :)");
    input.close();
    return clearInterval(game);
  }
}

let fetchUserInput = () => {
  input.question('1 -> left, 4 -> right, 2 -> up, 3 -> down\n', (answer) => {
    if(Object.keys(direction).indexOf(answer) !== -1) {
      addItems(answer);
      if(movePossible) {
        populatePlayground(playground);
        movePossible = false;
      }
      printPlayground(playground);
    }
    else {
      console.log("Incorrect input");
      printPlayground(playground);
    }
  });
  return;
}

generatePlaygroud(playground);
let game = setInterval(() => {fetchUserInput()}, 500);
